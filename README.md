# Assessment-NinjaStore

Execute Test From Terminal Using Below Maven Commands


*  To Run In Chrome
**mvn clean verify -DtagName=~@ignore -DBROWSER=chrome**


*  To Run In Firefox
**mvn clean verify -DtagName=~@ignore -DBROWSER=firefox**


*  To Run Registration Login Logout E2E Scenario In Chrome
**mvn clean verify -DtagName=@NinjaStoreRegistrationE2E -DBROWSER=chrome**

Post Execution Check Report In Folder
**target/cucumber-html-reports**