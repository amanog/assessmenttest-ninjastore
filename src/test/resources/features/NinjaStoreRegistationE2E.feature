@NinjaStoreRegistrationE2E
Feature: Ninja Store Acount Creation & Login Verification

  Background: 
    Given User is on The Ninja Store Homepage

  Scenario: New Registration
    When User navigates to Registration page
    And User fills up all the details on the page
    And User clicks On Continue
    Then User is displayed with message "Congratulations! Your new account has been successfully created!"
    And User do Logout
    And User navigates to login page
    And User enters Username and Password
    And User clicks on Login
    Then User is logged in successfully
