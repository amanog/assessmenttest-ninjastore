@NinjaStoreRegistration
Feature: Ninja New User Registration

  Background: 
    Given User is on The Ninja Store Homepage

  Scenario: New Registration
    When User navigates to Registration page
    And User fills up all the details on the page
    And User clicks On Continue
    Then User is displayed with message "Congratulations! Your new account has been successfully created!"

  Scenario: New Registration & Verify Incorrect Success Message
    When User navigates to Registration page
    And User fills up all the details on the page
    And User clicks On Continue
    Then User is displayed with message "Congratulations! Your new account has been unsuccessfully created!"
