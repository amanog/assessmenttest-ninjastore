package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import helpers.AppWait;
import helpers.Utils;
import stepDefinitions.Hooks;

public class NinjaLoginLogoutPage extends Utils {
	
	private RemoteWebDriver driver;

	public NinjaLoginLogoutPage(RemoteWebDriver driver) {
		this.driver = Hooks.driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "input-email")
	private WebElement emailAddressTextbox;

	@FindBy(id = "input-password")
	private WebElement passwordTextbox;

	@FindBy(xpath = "//input[@type='submit']")
	private WebElement loginButton;
	
	@FindBy(xpath = "//a[@title='My Account']")
	private WebElement myAccountButton;
	
	@FindBy(xpath = "//ul//a[text()='Login']")
	private WebElement myAccountLoginButton;
	
	@FindBy(xpath = "//ul//a[text()='Logout']")
	private WebElement logoutButton;
	
	@FindBy(xpath = "//h2[text()='My Account']")
	private WebElement myAccountHeader;
	
	
	@FindBy(xpath = "//div[@class='alert alert-danger alert-dismissible']")
	private WebElement invalidUsernamePasswordError;
	

	public WebElement getEmailAddressTextbox() {
        return AppWait.waitForElementToBeClickable(driver, emailAddressTextbox);
    }
	
	public WebElement getPasswordTextbox() {
        return AppWait.waitForElementToBeClickable(driver, passwordTextbox);
    }
	
	public WebElement getLoginButton() {
        return AppWait.waitForElementToBeClickable(driver, loginButton);
    }
	
	public WebElement getMyAccountButton() {
        return AppWait.waitForElementToBeClickable(driver, myAccountButton);
    }
	
	public WebElement getMyAccountLoginButton() {
        return AppWait.waitForElementToBeClickable(driver, myAccountLoginButton);
    }
	
	public WebElement getLogoutButton() {
        return AppWait.waitForElementToBeClickable(driver, logoutButton);
    }
	
	public WebElement getMyAccountHeader() {
        return AppWait.waitForElementToBeClickable(driver, myAccountHeader);
    }
	
	public WebElement getInvalidUsernamePasswordError() {
        return AppWait.waitForElementToBeClickable(driver, invalidUsernamePasswordError);
    }
}

