package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import helpers.AppWait;
import helpers.Utils;
import stepDefinitions.Hooks;

public class NinjaRegistrationPage extends Utils {

	private RemoteWebDriver driver;
	
	public NinjaRegistrationPage(WebDriver driver) {
		this.driver = Hooks.driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "input-firstname")
	private WebElement firstNameTextbox;

	@FindBy(id = "input-lastname")
	private WebElement lastNameTextbox;
	
	@FindBy(id = "input-email")
	private WebElement emailIDTextbox;

	@FindBy(id = "input-telephone")
	private WebElement mobileNumberTextbox;
	
	@FindBy(id = "input-password")
	private WebElement passwordTextbox;

	@FindBy(id = "input-confirm")
	private WebElement confirmPasswordTextbox;

	@FindBy(xpath = "//input[@name='agree']")
	private WebElement tncCheckBox;
	
	@FindBy(xpath = "//input[@value='Continue']")
	private WebElement continueButon;
	
	@FindBy(xpath = "//h1[text()='Account']/following-sibling::p[1]")
	private WebElement accountCreationSuccessMesage;
	
	@FindBy(xpath = "//a[text()='Continue']")
	private WebElement postAccountCreationcontinueButon;

	
	public WebElement getFirstNameTextbox() {
        return AppWait.waitForElementToBeClickable(driver, firstNameTextbox);
    }
	
	public WebElement getLastNameTextbox() {
        return AppWait.waitForElementToBeClickable(driver, lastNameTextbox);
    }
	
	public WebElement getEmailIDTextbox() {
        return AppWait.waitForElementToBeClickable(driver, emailIDTextbox);
    }
	
	public WebElement getMobileNumberTextbox() {
        return AppWait.waitForElementToBeClickable(driver, mobileNumberTextbox);
    }
	
	public WebElement getPasswordTextbox() {
        return AppWait.waitForElementToBeClickable(driver, passwordTextbox);
    }
	
	public WebElement getConfirmPasswordTextbox() {
        return AppWait.waitForElementToBeClickable(driver, confirmPasswordTextbox);
    }
	
	public WebElement getTNCCheckBox() {
        return AppWait.waitForElementToBeClickable(driver, tncCheckBox);
    }
	
	public WebElement getContinueButon() {
        return AppWait.waitForElementToBeClickable(driver, continueButon);
    }
	
	public WebElement getAccountCreationSuccessMesage() {
        return AppWait.waitForElementToBeVisible(driver, accountCreationSuccessMesage);
    }
	
	public WebElement getPostAccountCreationcontinueButon() {
        return AppWait.waitForElementToBeClickable(driver, postAccountCreationcontinueButon);
    }
}

