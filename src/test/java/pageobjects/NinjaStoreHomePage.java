package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import helpers.AppWait;
import helpers.Utils;
import stepDefinitions.Hooks;

public class NinjaStoreHomePage extends Utils {
	
	private RemoteWebDriver driver;

	public NinjaStoreHomePage(WebDriver driver) {
		this.driver = Hooks.driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//a[@title='My Account']")
	private WebElement myAccountButton;

	@FindBy(xpath = "//a[text()='Register']")
	private WebElement registerButton;
	
	@FindBy(xpath = "//a[text()='Login']")
	private WebElement loginButton;

	
	public WebElement getMyAccountButton() {
        return AppWait.waitForElementToBeClickable(driver, myAccountButton);
    }
	
	public WebElement getRegisterButton() {
        return AppWait.waitForElementToBeClickable(driver, registerButton);
    }
	
	public WebElement getLoginButton() {
        return AppWait.waitForElementToBeClickable(driver, loginButton);
    }

}

