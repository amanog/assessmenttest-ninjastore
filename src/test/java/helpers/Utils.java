package helpers;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Utils {
	
	private static Logger logger = LogManager.getLogger(Utils.class);
	public static Map<String, String> storeVariable = new HashMap<>();
	
	public enum ProjectConst {
		EXCEPTIONTEXT("**** Exception Occurred ****"),
		ELEMENTNOTFOUNDEXCEPTIONTXT("**** Element Not Found or Xpath String passed in null ****"),
		EXCEPTIONTEXTMETHOD("**** Exception Occurred in the Method ****"),
		VALUE(" ======> ");

		private String msg;

		ProjectConst(String s) {
			msg = s;
		}

		public String getMsg() {
			return msg;
		}
	}
	
	public static boolean clickElement(WebElement element, RemoteWebDriver driver)
	{
		boolean clickStatus = false;
		try {
			element.click();
			clickStatus = true;
		} catch (Exception e) {
			clickStatus = false;
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD + " " + element, e);
		}
		return clickStatus;
	}
	
	public static boolean sendText(WebElement element, String text, RemoteWebDriver driver)
	{
		boolean sendStatus = false;
		try {
			element.sendKeys(text);
			sendStatus = true;
		} catch (Exception e) {
			sendStatus = false;
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD + " " + element, e);
		}
		return sendStatus;
	}
	
	public static boolean checkForText(WebElement element, String textToCompare, RemoteWebDriver driver)
	{
		boolean textCheck = false;
		try {
			String acutalText = element.getText();
			
			if (acutalText.equals(textToCompare)) {
				textCheck = true;
			}
		} catch (Exception e) {
			textCheck = false;
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD + " " + element, e);
		}
		return textCheck;
	}
	
	public static boolean checkForElement(WebElement element, RemoteWebDriver driver) {
		
		boolean elementCheck = false;
		
		try {
			logger.info("Checking for element present: ");	
			if (element.isDisplayed()) {
				
				scrollToElement(element, driver);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: 5px solid red;");
				elementCheck = true;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");
				logger.info("Element is present");
			} else {
				elementCheck = false;
				logger.info("Element is not present");
			}
		} catch (NoSuchElementException e) {
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD + " " + element, e);
		}
		return elementCheck;
	}
	
	public void clickCheckbox(WebElement element, String CheckOrUncheck, RemoteWebDriver driver)
	{
		boolean check;
		
		if (CheckOrUncheck.equalsIgnoreCase("Check")) {
			check = true;
		} else {
			check = false;
		}
		
		if (!check && element.isSelected()) {
			element.click();
		}
		else if (check && !element.isSelected()) {
			element.click();
		}
	}
	
	public static void scrollToElement(WebElement element, RemoteWebDriver driver) {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		} catch (Exception e) {
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + " " + "Error while scrollToElement", e);
			throw e;
		}
	}

}