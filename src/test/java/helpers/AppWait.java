package helpers;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.time.Duration;

public class AppWait {
    
    private static final int DEFAULT_WAIT_4_ELEMENT = 20;
    private static Wait<RemoteWebDriver> wait;
    private static Logger logger = LogManager.getLogger(Wait.class);

    
    private static Wait<RemoteWebDriver> getWait(RemoteWebDriver driver) {
        if (wait == null) {
            wait = new FluentWait<>(driver).withTimeout(Duration.ofSeconds(30)).pollingEvery(Duration.ofSeconds(3))
                    .ignoring(NoSuchElementException.class);
        }
        return wait;
    }
    
    public static WebElement waitForElementToBeClickable(RemoteWebDriver driver, WebElement element) {
        try {
        	logger.info("Wait for element" + element + " to be clickable");
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
            getWait(driver).until(ExpectedConditions.elementToBeClickable(element));
            driver.manage().timeouts().implicitlyWait(DEFAULT_WAIT_4_ELEMENT, TimeUnit.SECONDS);
        } catch (NoSuchElementException e) {
        	logger.error(e.getStackTrace());
        }
        return element;
    }
    
    public static WebElement waitForElementToBeVisible(RemoteWebDriver driver, WebElement element) {
        try {
        	logger.info("Wait for element" + element + " to be visible");
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
            getWait(driver).until(ExpectedConditions.visibilityOf(element));
            driver.manage().timeouts().implicitlyWait(DEFAULT_WAIT_4_ELEMENT, TimeUnit.SECONDS);
        } catch (NoSuchElementException e) {
        	logger.error(e.getStackTrace());
        }
        return element;
    }
    
}