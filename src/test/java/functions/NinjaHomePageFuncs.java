package functions;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

import pageobjects.NinjaStoreHomePage;

public class NinjaHomePageFuncs extends NinjaStoreHomePage {
	
    private static Logger logger = LogManager.getLogger(NinjaHomePageFuncs.class);
    private RemoteWebDriver driver;

    public NinjaHomePageFuncs(RemoteWebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public void navigateToRegistrationPage() {
        try {
            logger.info("Navigating To Registration Page");
            clickElement(getMyAccountButton(), driver);
            clickElement(getRegisterButton(), driver);
             
        } catch (Exception e) {
            
        }
    }
}
