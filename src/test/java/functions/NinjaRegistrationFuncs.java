package functions;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.remote.RemoteWebDriver;

import pageobjects.NinjaRegistrationPage;

public class NinjaRegistrationFuncs extends NinjaRegistrationPage {

	private static Logger logger = LogManager.getLogger(NinjaRegistrationFuncs.class);
	private RemoteWebDriver driver;

	public NinjaRegistrationFuncs(RemoteWebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	public void fillRegistationForm() {
		try {
			logger.info("Filling Up Registration Form");
			sendText(getFirstNameTextbox(), "TEST", driver);
			sendText(getLastNameTextbox(), RandomStringUtils.randomAlphabetic(5).toUpperCase(), driver);

			storeVariable.put("EMAIL_ID", RandomStringUtils.randomAlphabetic(5).toLowerCase()+"@abc.com");
			sendText(getEmailIDTextbox(), storeVariable.get("EMAIL_ID"), driver);

			sendText(getMobileNumberTextbox(), "01661"+RandomStringUtils.randomNumeric(5), driver);

			storeVariable.put("PASSWORD", RandomStringUtils.randomNumeric(10));
			sendText(getPasswordTextbox(), storeVariable.get("PASSWORD"), driver);
			sendText(getConfirmPasswordTextbox(), storeVariable.get("PASSWORD"), driver);

			clickCheckbox(getTNCCheckBox(), "Check", driver);

		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}

	public void clickOnContinue() {
		try {
			logger.info("Clicking Continue");
			clickElement(getContinueButon(), driver);

		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public void checkSuccessfullAccountCreation(String successMessage) {
		try {
			logger.info("Checking Successfull Account Creation");
			Assert.assertTrue("Account Created Successfully", checkForText(getAccountCreationSuccessMesage(), successMessage, driver));

		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
}
