package functions;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.remote.RemoteWebDriver;

import pageobjects.NinjaLoginLogoutPage;
import pageobjects.NinjaRegistrationPage;

public class NinjaLoginLogoutFuncs extends NinjaLoginLogoutPage {

	private static Logger logger = LogManager.getLogger(NinjaLoginLogoutFuncs.class);
	private RemoteWebDriver driver;

	public NinjaLoginLogoutFuncs(RemoteWebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	public void doLogout() {
		try {
			clickElement(getMyAccountButton(), driver);
			clickElement(getLogoutButton(), driver);

		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public void navigateToLoginPage() {
		try {
			clickElement(getMyAccountButton(), driver);
			clickElement(getMyAccountLoginButton(), driver);

		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public void enterUsernamePasswordLogin(String emailID, String password) {
		try {
			sendText(getEmailAddressTextbox(), emailID, driver);
			sendText(getPasswordTextbox(), password, driver);

		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public void clickOnLoginButton() {
		try {
			clickElement(getLoginButton(), driver);

		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public void checkSuccessfullLogin() {
		try {
			checkForElement(getMyAccountHeader(), driver);

		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
}
