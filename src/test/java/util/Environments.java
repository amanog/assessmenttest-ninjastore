package util;

import java.util.logging.Level;

import org.openqa.selenium.remote.RemoteWebDriver;

import stepDefinitions.Hooks;

public class Environments {

    public  Environments() throws InterruptedException {

    	RemoteWebDriver driver = Hooks.driver;
    	
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");

        String env = System.getProperty("ENV");

            if (env==null){
                env = "qa";
        }

        System.out.println("The test is running on " + env.toUpperCase() + " environment");
        switch (env.toLowerCase()) {

            case "dev":
                driver.get("http://tutorialsninja.com/demo/");
                break;

            case "qa":
                driver.get("http://tutorialsninja.com/demo/");
                break;

        }
    }
}
