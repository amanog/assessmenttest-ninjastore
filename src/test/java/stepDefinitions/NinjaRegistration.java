package stepDefinitions;

import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import functions.NinjaRegistrationFuncs;

public class NinjaRegistration {

	private RemoteWebDriver driver = Hooks.driver;

	NinjaRegistrationFuncs registrationFuncs = new NinjaRegistrationFuncs(driver);

	@And("^User fills up all the details on the page$")
    public void user_fills_up_all_the_details_on_the_page() {
		registrationFuncs.fillRegistationForm();
    }
	
	@And("^User clicks On Continue$")
    public void click_on_continue() {
		registrationFuncs.clickOnContinue();
    }
	
	@Then("^User is displayed with message \"([^\"]*)\"$")
    public void user_is_displayed_with_message_something(String successMessage) {
		registrationFuncs.checkSuccessfullAccountCreation(successMessage);	
    }

}
