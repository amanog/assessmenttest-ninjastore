package stepDefinitions;

import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import functions.NinjaHomePageFuncs;
import functions.NinjaLoginLogoutFuncs;
import helpers.Utils;

public class NinjaLoginLogout {

	private RemoteWebDriver driver = Hooks.driver;

	NinjaLoginLogoutFuncs loginLogoutFuncs = new NinjaLoginLogoutFuncs(driver);

    @And("^User do Logout$")
    public void user_do_logout() {
    	loginLogoutFuncs.doLogout();
    }

    @And("^User navigates to login page$")
    public void user_navigates_to_login_page() {
    	loginLogoutFuncs.navigateToLoginPage();
    }

    @And("^User enters Username and Password$")
    public void user_enters_username_and_password() {
    	loginLogoutFuncs.enterUsernamePasswordLogin(Utils.storeVariable.get("EMAIL_ID"), Utils.storeVariable.get("PASSWORD"));
    }

    @And("^User clicks on Login$")
    public void user_clicks_on_login() {
    	loginLogoutFuncs.clickOnLoginButton();
    }
    
    @Then("^User is logged in successfully$")
    public void user_is_logged_in_successfully() throws InterruptedException {
    	loginLogoutFuncs.checkSuccessfullLogin();
    }

}
