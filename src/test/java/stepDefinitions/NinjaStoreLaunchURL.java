package stepDefinitions;

import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.java.en.Given;
import functions.NinjaStoreLaunchURLFuncs;

public class NinjaStoreLaunchURL {
	
	private RemoteWebDriver driver = Hooks.driver;
	
	NinjaStoreLaunchURLFuncs launchURL = new NinjaStoreLaunchURLFuncs(driver);

	@Given("^User is on The Ninja Store Homepage$")
    public void user_is_on_the_ninja_store_homepage() throws InterruptedException {
		launchURL.navigateToNinjaStorePage();
	}

}
