package stepDefinitions;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import helpers.JunitParallel;
import org.junit.runner.RunWith;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import util.Browsers;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

@RunWith(JunitParallel.class)
public class Hooks {

    public static RemoteWebDriver driver;
    private static String driverDirectory = System.getProperty("user.dir") + "/webDrivers/usr/bin";
    private ChromeOptions chromeOptions = new ChromeOptions();
    private InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
    public static String downloadfolderPath = System.getProperty("user.dir") + "/testData/testDataOutput";
    private DesiredCapabilities capabilities = new DesiredCapabilities();
    private Browsers browsers = new Browsers();


    @Before
    public void openBrowser() throws Exception {

        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
        String os = System.getProperty("os.name").toLowerCase();

        String browser = System.getProperty("browser".toUpperCase());
        if (browser == null) {
             browser = "chrome";
        }
        
        switch (browser.toLowerCase()) {

            case "chrome":
                HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
                chromePrefs.put("download.default_directory", downloadfolderPath);
                chromeOptions.setExperimentalOption("prefs", chromePrefs);
                // String os = System.getProperty("os.name").toLowerCase();

                if (os.contains("win")) {
                    System.setProperty("webdriver.chrome.driver", driverDirectory + "/chrome/chromedriver.exe");
                    driver = new ChromeDriver(chromeOptions);
                    driver.manage().window().maximize();
                    driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
                } else {

                    System.setProperty("webdriver.chrome.driver", driverDirectory + "/chrome/chromedriver");
                    chromeOptions.addArguments("--start-maximized");
                    driver = new ChromeDriver(chromeOptions);
                    driver.manage().window().setSize(new Dimension(1280, 1024));
                }
                break;

            case "firefox":
                if (os.contains("win")) {
                    System.setProperty("webdriver.gecko.driver", driverDirectory + "/geckoFirefox/geckodriver.exe");
                    driver = new FirefoxDriver();
                    driver.manage().window().maximize();
                }
                else{
                    System.setProperty("webdriver.gecko.driver", driverDirectory + "/geckoFirefox/geckodriver");
                    driver = new FirefoxDriver();
                    driver.manage().window().setSize(new Dimension(1280, 1024));
                }
                break;

            case "ie":
                System.setProperty("webdriver.ie.driver", driverDirectory + "/IEDriver/IEDriverServer.exe");
                InternetExplorerOptions ieOptions = new InternetExplorerOptions().requireWindowFocus();
                ieOptions.destructivelyEnsureCleanSession();
                driver = new InternetExplorerDriver(ieOptions);
                driver.manage().window().setSize(new Dimension(1280, 1024));
                break;
        }

        System.out.println("The Browser used for this test is: " + browser.toUpperCase());

    }


    @After
    public void embedScreenshot(Scenario scenario) throws Exception {
        System.out.println(scenario.getStatus()+"\t"+Arrays.asList(scenario.getSourceTagNames()).toString());
        if (scenario.isFailed()) {
            try {
                scenario.write("Current Page URL is " + new URL(driver.getCurrentUrl()));
                byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            } catch (WebDriverException somePlatformsDontSupportScreenshots) {
                System.err.println(somePlatformsDontSupportScreenshots.getMessage());
            }
        }
        if (driver != null) {
            driver.quit();
        }
    }
}