package stepDefinitions;

import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.java.en.When;
import functions.NinjaHomePageFuncs;

public class NinjaHomePage {

	private RemoteWebDriver driver = Hooks.driver;

	NinjaHomePageFuncs homeFuncs = new NinjaHomePageFuncs(driver);

	@When("^User navigates to Registration page$")
    public void user_navigates_to_registration_page() {
		homeFuncs.navigateToRegistrationPage();
    }

}
